<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#"  Inherits="ns_CalendarWorkorder.CalendarWorkorder" enableEventValidation="false" EnableSessionState="true" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
    <meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><%--FB 2779--%>
	<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <script src="script/mytreeNET.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
        parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
    </script>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <script type="text/javascript" src="inc/functions.js"></script>	
    <script type="text/javascript" src="script/mousepos.js"></script>
    <script type="text/javascript" src="script/showmsg.js"></script>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script>
    <script type="text/javascript" src="script/calview.js"></script><%--ZD 100428--%>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="script/Workorder.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>
	<html>
	<head>
	    <script language="javascript">
	        //ZD 100604 start
	        var img = new Image();
	        img.src = "../en/image/wait1.gif";
	        //ZD 100604 End
	        function ViewDetails(e)
	        {
	          //Fogbugz 1113 - Start
	          //ViewWorkorderDetails(e.value(), e.tag("ConfID"));
	          ViewWorkorderDetails(e.value(), e.tag("ConfID"),queryField("t"));
	          //Fogbugz 1113 - End
	        }
	        function getUsers(evt)
	        {
                var obj;
                if (window.event != window.undefined)
                   obj =  window.event.srcElement;
                else
                   obj = evt.target; 
                   
                var treeNodeFound = false;
                var checkedState; 
                
                if (obj.tagName == "INPUT" && obj.type == "checkbox") 
                {    //AddResource(obj.name, obj.id, AddResource_CallBack);
                        alert(obj.parentNode.title);
                }
            }
            function showcalendar()
            {
                if (document.getElementById ("flatCalendarDisplay").style.display == "")
	                document.getElementById ("flatCalendarDisplay").style.display = "none";
                else
	                document.getElementById ("flatCalendarDisplay").style.display = "";
            }

	        function initsubtitle ()
            {
                var dd, m = "";

                var varjan = RSJan; varFeb = RSFeb; varMar = RSMar; varApr = RSApr; varMay = RSMay; varJun = RSJun; varJul = RSJul; varAug = RSAug;
                var varSep = RSSep; varOct = RSOct; varNov = RSNov; varDec = RSDec;
                var varDS = DateSelector;

                var weekday=new Array(7)
                weekday[0] = RSSunday;
                weekday[1] = RSMonday;
                weekday[2] = RSTuesday;
                weekday[3] = RSWednesday;
                weekday[4] = RSThursday;
                weekday[5] = RSFriday;
                weekday[6] = RSSaturday;

                //var months = new Array("January", "February", "March", varjan, "May", "June", "July", "August", "September", "October", "November", "December")
                var months = new Array(varjan, varFeb, varMar, varApr, varMay, varJun, varJul, varAug, varSep, varOct, varNov, varDec)
                //ZD 100420
                var strcalicon = "<img src='image/calendar.gif' alt='Date Selector' border='0' width='20' height='20' id='cal_trigger' style='cursor: pointer;' title='" + DateSelector + "' onclick='showcalendar();' />"; //Edited For FF... //ZD 100419

                dd = new Date(confdate);
                m += weekday[dd.getDay()] + ", " + months[dd.getMonth()] + " " + dd.getDate() + ", " + dd.getFullYear();
                startDate = (dd.getMonth() + 1) + "/" + dd.getDate() + "/" + dd.getFullYear();
                //alert('<%=Session["systemTimezone"] %>');
                //Edited For FF...
                document.getElementById("subtitlenamediv").innerHTML = "<table><tr><td><a href='javascript: void(0)'  style='cursor: default;color=blue'><span style='font-size: 14px; color=blue; font-weight: bold;'><u>" + m + ' <%=Session["systemTimezone"] %>' + "</u></span></a></td>" +  // ZD 102495
	                "<td>" + strcalicon + "</td></tr></table>";
            }
            function datechg() {
                //alert(confdate);
                DataLoading("1");
                document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	            document.getElementById ("flatCalendarDisplay").style.display = "none";
	            initsubtitle();
                document.getElementById("__EVENTTARGET").value="ChangeDate";
                __doPostBack("ChangeCalendarDate", document.getElementById("__EVENTTARGET").value);
            }
            function DataLoading(val)
            {
                if (val=="1")
                    document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
                    //document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' >"; // FB 2742
                else
                    document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678                  
            }
	    </script>
	</head>
	<body>
<center>
    <h3><asp:Literal Text="<%$ Resources:WebResources, CalendarWorkorder_WorkorderCalen%>" runat="server"></asp:Literal></h3>
</center>

<form id="frmCalendar" runat="server">
 <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
                <div id="divButton" nowrap="nowrap" >
                    <%--code added for Soft Edge button--%><%--ZD 100420 Starts--%>
                    
                    <button id="btnDaily" runat="server" onserverclick="ChangeView" style="Width:80px" ><asp:Literal Text="<%$ Resources:WebResources, CalendarWorkorder_btnDaily%>" runat="server"></asp:Literal></button> <%--ZD 100393--%>
                    <button id="btnWeekly" runat="server" onserverclick="ChangeView" style="Width:110px"  ><asp:Literal Text="<%$ Resources:WebResources, CalendarWorkorder_btnWeekly%>" runat="server"></asp:Literal></button> <%--ZD 100393--%>
                    <button id="btnMonthly" runat="server" onserverclick="ChangeView" style="Width:80px"  ><asp:Literal Text="<%$ Resources:WebResources, CalendarWorkorder_btnMonthly%>" runat="server"></asp:Literal></button> <%--ZD 100393--%>
                    <%--<asp:Button ID="btnDaily" Text="Daily" OnCommand="ChangeView" runat="server" CommandArgument="1" Width="80px" />--%><%--FB 2664 --%>
                    <%--<asp:Button ID="btnWeekly" Text="Weekly" OnCommand="ChangeView" runat="server" CommandArgument="2" Width="80px" />--%><%--FB 2664 --%>
                    <%--<asp:Button ID="btnMonthly" Text="Monthly" OnCommand="ChangeView" runat="server" CommandArgument="3" Width="80px" />--%><%--FB 2664 --%>
                    <%--ZD 100420 End--%>
                </div>
    <table cellspacing="0" cellpadding="1" border="0" width="100%">
        <tr>
            <td align="center" colspan="2">
                <div id="subtitlenamediv"  style="margin-left:80px"></div> <%--ZD 100393--%>
                <div id="dataLoadingDIV" align="center"  style="display:none">
                    <img border='0' src='image/wait1.gif'  alt='Loading..' />
                </div> <%--ZD 100176--%><%--ZD 100678--%>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table><tr><td><div id="flatCalendarDisplay" style="z-index:999;cursor:pointer"></div></td></tr></table>
            </td>
        </tr>
        <tr>
            <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, CalendarWorkorder_LastNamebegin%>" runat="server"></asp:Literal></td>
            <td align="center">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%" valign="top">
                <asp:Panel runat="server" ID="pnlUsers" Height="600" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0">
                    <table border="0" width="332" class="treeSelectedNode">
                        <tr>
                            <td valign="top" width="25%">
                                <table width="100%" >
                                    <tr>
                                        <td align="center" width="15%"><asp:LinkButton ID="LinkButton1" runat="server" Text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton1%>" CommandArgument="a" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="btnAlpha" runat="server" Text="<%$ Resources:WebResources, CalendarWorkorder_btnAlpha%>" CommandArgument="A" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton ID="LinkButton2" runat="server" Text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton2%>" CommandArgument="B" CommandName="User" OnCommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton3" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton3%>" commandargument="C" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton4" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton4%>" commandargument="D" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton5" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton5%>" commandargument="E" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton6" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton6%>" commandargument="F" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton7" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton7%>" commandargument="G" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton8" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton8%>" commandargument="H" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton9" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton9%>" commandargument="I" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton10" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton10%>" commandargument="J" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton11" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton11%>" commandargument="K" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton12" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton12%>" commandargument="L" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton13" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton13%>" commandargument="M" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton14" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton14%>" commandargument="N" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton15" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton15%>" commandargument="O" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton16" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton16%>" commandargument="P" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton17" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton17%>" commandargument="Q" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton18" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton18%>" commandargument="R" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton19" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton19%>" commandargument="S" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton20" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton20%>" commandargument="T" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton21" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton21%>" commandargument="U" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton22" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton22%>" commandargument="V" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton23" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton23%>" commandargument="W" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton24" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton24%>" commandargument="X" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton25" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton25%>" commandargument="Y" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td width="15%" align="center"><asp:LinkButton id="LinkButton26" runat="server" text="<%$ Resources:WebResources, CalendarWorkorder_LinkButton26%>" commandargument="Z" commandname="User" oncommand="GetUsers"></asp:LinkButton></td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" class="blackblodtext">
                                <asp:CheckBoxList AutoPostBack="true" ID="lstUsers" runat="server" OnSelectedIndexChanged="GetUserWorkOrders" onclick="javascript:DataLoading(1)"></asp:CheckBoxList>
                                <asp:Label runat="server" ID="tblNoUsers" CssClass="lblError" Text="<%$ Resources:WebResources, CalendarWorkorder_tblNoUsers%>" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Table ID="tblPaging" runat="server" Width="100%" >
                                    <asp:TableRow Width="100%" >
                                        <asp:TableCell Width="100%"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                    <asp:TextBox ID="txtAlpha" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtPageNo" runat="server" Visible="false"></asp:TextBox>
                </asp:Panel>
            </td>
            <td valign="top" align="left"><%--FB 2992--%>
                <div style="vertical-align:middle;Width:98%" >
                <DayPilot:DayPilotScheduler CssClass="tableHeader" RowHeaderWidth="200" ID="schDaypilot" runat="server" Days="1" Visible="true"
                    DataStartField="start" DataEndField="end" DataTextField="Name" DataValueField="ID" DataTagFields="ConfID,ToolText"
                    HeaderFontSize="8pt" HeaderHeight="17" DataResourceField="user" EventHeight="75" BubbleID="Details" ShowToolTip="false" 
                    CellDuration="30" CellWidth="20" CellGroupBy="Hour" EventClickHandling="JavaScript"
                    EventResizeHandling="CallBack" EventClickJavaScript="ViewDetails(e);" Layout="TableBased"
                    ClientObjectName="dps1" Width="900" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender"/><%--FB 2087--%>
                <DayPilot:DayPilotBubble ID="Details" runat="server" Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                </div>
                <asp:Label ID="lblNoUsers" Text="<%$ Resources:WebResources, CalendarWorkorder_lblNoUsers%>" runat="server" CssClass="lblError" Visible="true"></asp:Label>
            </td>
        </tr>
    </table>  
    <table width="100%" > <%--ZD 100428--%>
      <tr align="center">
        <td align="center">
        <button id="btnCancel" runat="server" onserverclick="btnCancel_Click" class="altLongBlueButtonFormat" ><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
        </td>
      </tr>
    </table>
    <asp:TextBox ID="txtType" Visible="false" runat="server"></asp:TextBox>  
    <input type="hidden" ID="txtSelectedDate" runat="server" />
    <input type="hidden" ID="startDate" runat="server" /><%--FB 2305--%>
    <input type="hidden" ID="endDate" runat="server" />
    <input type="hidden" ID="view" runat="server" />
</form>
<script language="javascript">

var curdate = new Date();

if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
	showFlatCalendar(0, '%m/%d/%Y');
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value);
}
document.getElementById ("flatCalendarDisplay").style.position = 'absolute';
document.getElementById ("flatCalendarDisplay").style.top = 200;
document.getElementById ("flatCalendarDisplay").style.height = 150;
document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
document.getElementById ("flatCalendarDisplay").style.display = "none";
document.getElementById("divButton").style.position = 'absolute';
document.getElementById("divButton").style.top = 150;
document.getElementById("divButton").style.height = 150;
document.getElementById("divButton").style.left = 10; // FB 2050 //FB 2897
document.getElementById("divButton").style.width = "auto"; // FB 2050 //ZD 100420

DataLoading("0");
initsubtitle();
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    function EscClosePopup() {
        if (document.getElementById('flatCalendarDisplay') != null) {
            document.getElementById('flatCalendarDisplay').style.display = "none";
        }
    }
</script>
<%--ZD 100428 END--%>
</body></html> 
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
	<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->